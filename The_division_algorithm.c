// Name: The_division_algorithm.c
// Time: 08:06 11.16.2018 (KST)
// Author: Azimjon Kamolov
// Contact: azimjon.6561@gmail.com
// Purpose: to prove the division algorithms
	
/* Let a be an integer and d a positive integer. Then there are 
unique integers q and r, with 0 = r < d, such that a = dq + r.*/
#include<stdio.h>

int main()
{
	int a, d, r, q, i=0, sum=0, n;
	printf("Give by order please: ");
	scanf("%d %d", &a, &d);				// TO GET INPUTS
	n=a-d;								// TO ENABLE THE PROGRAM WHERE IT NEEDS TO
	if(a>0 && d>0) 						// TO CHECK IF THEY ARE POSITIVE INTS
	{
		while(sum<n) 					// RUNS UNTIL SUM IS SMALLER THAN N
		{
			sum=d*i; 					// TO GET SUM OF D*I
			printf("sum %d\n", sum);
			i++; 						// TO INCREASE I EACH TIME
		}
		r=a-sum;						// TO GET R
		if(d==r)    					// IF R IS EQUAL TO D 
		{
			r=0;						// THEN MAKE IT 0
		}
		else
		{
			i-=1;						// IF NOT THEN, I-1 TO GET A CORRECT Q
		}
		q=i;							// TO GET Q
	}
	printf("a = %d, d=%d, q=%d, r=%d\n", a,d,q,r); // TO SHOW
	printf("Proof: ");
	printf("%d=%d(%d)+%d\n", a=d*q+r, d,q,r); 	// TO PROVE
	return 0;

}
